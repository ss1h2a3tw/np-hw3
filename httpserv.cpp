#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

int listenfd;

const string document_root="/webroot";

string parsereq(const string& req,string& query){
    stringstream ss;
    ss << req;
    string method,uri,version;
    ss >> method >> uri >> version;
    if(method!="GET")throw("Not GET");
    if(uri.find("http")!=string::npos){
        size_t pos=0;
        for(int i = 0 ; i < 3 ; i ++){
            if(pos==string::npos)throw("Error Parsing URI");
            pos=uri.find_first_of("/",pos+1);
        }
        if(pos==string::npos)throw("Error Parsing URI");
        uri=uri.substr(pos);
    }
    if(uri.find("?")!=string::npos){
        size_t pos=uri.find("?");
        query=uri.substr(pos+1);
        return uri.substr(0,pos);
    }
    else{
        return uri;
    }
}

void processhttp(int fd,const char* addr){
    try{
        signal(SIGINT,[](int){
            shutdown(STDIN_FILENO,SHUT_RDWR);
            shutdown(STDOUT_FILENO,SHUT_RDWR);
            exit(EXIT_SUCCESS);
        });
        if(dup2(fd,STDIN_FILENO)==-1)throw("Error in dup2()");
        if(dup2(fd,STDOUT_FILENO)==-1)throw("Error in dup2()");
        close(fd);
        string in;
        string request;
        getline(cin,request);
        if(request.back()=='\r')request.pop_back();
        cerr << request << endl;
        string query;
        string path=parsereq(request,query);
        cerr << "PATH: " << path << " QUERY: " << query << endl;
        map<string,string> para;
        while(!cin.eof()){
            getline(cin,in);
            if(in.back()=='\r')in.pop_back();
            cerr << in << endl;
            if(in.size()==0)break;
            auto pos=in.find(": ");
            if(in.find(": ")==string::npos)throw("Failed to parse Request Header");
            para[in.substr(0,pos)]=in.substr(pos+2);
        }
        cout << "HTTP/1.1 200 OK\r\n" << flush;
        char tmp[4096]={0};
        getcwd(tmp,4095);
        string exepath=string(tmp)+document_root+path;
        if(path.rfind(".cgi")+4!=path.size()){
            cout << "\r\n";
            ifstream fin;
            fin.open(exepath);
            fin >> noskipws ;
            while(!fin.eof()){
                char c;
                fin >> c;
                cout << c;
            }
            cout << flush;
        }
        else{
            if(setenv("QUERY_STRING",query.c_str(),1)==-1)throw("Error in setenv()");
            if(setenv("CONTENT_LENGTH","0",1)==-1)throw("Error in setenv()");
            if(setenv("REQUEST_METHOD","GET",1)==-1)throw("Error in setenv()");
            if(setenv("SCRIPT_NAME",path.c_str(),1)==-1)throw("Error in setenv()");
            if(setenv("REMOTE_HOST","example.com",1)==-1)throw("Error in setenv()");
            if(setenv("REMOTE_ADDR",addr,1)==-1)throw("Error in setenv()");
            if(setenv("AUTH_TYPE","BASIC",1)==-1)throw("Error in setenv()");
            if(setenv("REMOTE_USER","USER",1)==-1)throw("Error in setenv()");
            if(setenv("REMOTE_IDENT","IDENT",1)==-1)throw("Error in setenv()");
            string dir=exepath.substr(0,exepath.find_last_of("/"));
            chdir(dir.c_str());
            memset(&tmp,0,sizeof(tmp));
            strncpy(tmp,exepath.c_str(),4095);
            cerr << tmp << endl;
            execl(exepath.c_str(),exepath.c_str());
            throw("Error in exec");
        }
    }
    catch (const char* err){
        cout << "HTTP/1.1 400 Bad Request\r\n";
        cout.flush();
        cerr << err << endl;
        exit(EXIT_FAILURE);
    }
    //Prevent it doesnt exit
    exit(EXIT_FAILURE);
}

int main (){
    try{
        signal(SIGCHLD,SIG_IGN);
        signal(SIGINT,[](int){
            shutdown(listenfd,SHUT_RDWR);
            exit(EXIT_SUCCESS);
        });
        listenfd=socket(AF_INET,SOCK_STREAM,0);
        const int enable = 1;
        if(setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(enable))==-1)throw("Error setting socket");
        sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(8790);
        addr.sin_addr.s_addr=INADDR_ANY;
        if(listenfd==-1)throw("Error opening socket");
        if(bind(listenfd,(sockaddr*)&addr,sizeof(sockaddr_in))==-1)throw("Failed to bind socket");
        while(1){
            listen(listenfd,10);
            memset(&addr,0,sizeof(sockaddr_in));
            socklen_t clilen=sizeof(sockaddr_in);
            int newfd=accept(listenfd,(sockaddr*)&addr,&clilen);
            if(newfd==-1)throw("Failed to accept new client");
            cout << "New user with fd: " << newfd << " From:" << inet_ntoa(addr.sin_addr) << ":" << ntohs(addr.sin_port) << endl;
            pid_t pid=fork();
            if(pid==-1)throw("Failed to fork");
            if(pid==0){
                close(listenfd);
                processhttp(newfd,inet_ntoa(addr.sin_addr));
            }
            else{
                close(newfd);
            }
        }
    }
    catch(const char* err){
        cerr << err << endl;
    }
}
