#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
using namespace std;

const size_t query_length=65536;
const size_t readbuf_len=65536;
const size_t writebuf_len=65536;
int cnt;

enum SOCKSTATE{CONNECTING,WRITING,READING,DONE};

struct conn{
    string host;
    uint16_t port;
    string batch_file;
    int fd;
    SOCKSTATE state;
    fstream fin;
    char writebuf[writebuf_len];
    size_t writesize;
    size_t writeidx;
    conn() = default;
    conn(conn&&) = default;
};

vector<conn> connects;

void parsequery(){
    char query[query_length];
    if(!getenv("QUERY_STRING"))return;
    strncpy(query,getenv("QUERY_STRING"),query_length);
    map<string,string> para;
    query[query_length-1]=0;
    {
        size_t pre = 0;
        auto len = strlen(query);
        query[len]='&';
        for(size_t i = 0 ; i <= len ; i ++){
            if(query[i]=='&'){
                for(size_t j = pre ; j < i ; j ++){
                    if(query[j]=='='){
                        query[j]=0;
                        query[i]=0;
                        para[string(query+pre)]=string(query+j+1);
                        break;
                    }
                }
                pre=i+1;
            }
        }
    }
    for(int i = 1 ; i <= 5; i ++){
        const string h=string("h")+to_string(i);
        const string p=string("p")+to_string(i);
        const string f=string("f")+to_string(i);
        if(para.count(h)&&para.count(p)&&para.count(f)){
            conn tmp;
            tmp.host=para[h];
            tmp.port=stoi(para[p]);
            tmp.batch_file=para[f];
            connects.push_back(move(tmp));
        }
    }
}

void printheader(){
    cout << "Status: 200\r\n";
    cout << "Content-Type: text/html\r\n\r\n";
    cout << "\
<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\
<title>Network Programming Homework 3</title></head><body bgcolor=#336699>\
<font face=\"Courier New\" size=2 color=#FFFF99>";
    cout << flush;
}

void printbody(){
    cout <<"<table width=\"800\" border=\"1\"><tr>";
    for(const auto& con:connects){
        cout << "<td>" << con.host << "</td>";
    }
    cout << "</tr><tr>";
    for(size_t i = 0 ; i < connects.size() ; i ++){
        cout << "<td valign=\"top\" id=\"m" << i << "\"></td>";
    }
    cout << "</tr></table>";
    cout << flush;
}

void printfooter(){
    cout << "</font></body></html>";
    cout << flush;
}

void print(const char* str,int id,bool bold){
    cout << "<script>document.all['m" << id << "'].innerHTML += \"";
    if(bold)cout << "<b>";
    for(size_t i = 0 ; i < strlen(str) ; i ++){
        if(str[i] == '\n') cout << "<br>";
        else if(str[i] == '\r');
        else if(str[i] == '<') cout << "&lt;";
        else if(str[i] == '>') cout << "&gt;";
        else if(str[i] == '&') cout << "&amp;";
        else if(str[i] == '"') cout << "&quot;";
        else if(str[i] == ' ') cout << "&nbsp;";
        else cout << str[i];
    }
    if(bold)cout << "</b>";
    cout << "\";</script>" << flush;
}

void setupconnections(){
    for(auto& now:connects){
        now.fd=socket(AF_INET,SOCK_STREAM|SOCK_NONBLOCK,0);
        if(now.fd==-1)throw("Failed to create socket");
        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = htons(now.port);
        if(inet_aton(now.host.c_str(),&(addr.sin_addr))==0)throw("Failed to set host IP");
        if(connect(now.fd,(sockaddr*)&addr,sizeof(addr))==-1){
            if(errno!=EINPROGRESS)throw("Failed in connecting");
            now.state=CONNECTING;
        }
        else now.state=READING;
        now.fin.open(now.batch_file,fstream::in);
        now.writeidx=0;
        now.writesize=0;
    }
}

void handlesockets(){
    fd_set rfds;
    fd_set wfds;
    int nfds=0;
    for(const auto& con:connects){
        nfds=max(nfds,con.fd);
    }
    nfds++;
    while(1){
        bool done=true;
        for(const auto& con:connects){
            if(con.state!=DONE)done=false;
        }
        if(done)break;
        FD_ZERO(&rfds);
        FD_ZERO(&wfds);
        for(const auto& con:connects){
            if(con.state==CONNECTING||con.state==WRITING)FD_SET(con.fd,&wfds);
            if(con.state==CONNECTING)cerr << "Setting Connecting" << endl;
            else if(con.state==READING) FD_SET(con.fd,&rfds);
        }
        if(select(nfds,&rfds,&wfds,(fd_set*)nullptr,(timeval*)nullptr)<0)
            throw("Error in Select");
        for(size_t i = 0 ; i < connects.size() ; i ++){
            auto& now=connects[i];
            if(now.state==CONNECTING&&FD_ISSET(now.fd,&wfds)){
                int error;
                socklen_t len=sizeof(int);
                if(getsockopt(now.fd,SOL_SOCKET,SO_ERROR,&error,&len)<0||error<0)
                    throw("Failed in connecting");
                now.state=READING;
                cerr << "connected " << i << endl;

            }
            else if(now.state==WRITING&&FD_ISSET(now.fd,&wfds)){
                if(now.writeidx>=now.writesize){
                    now.writeidx=0;
                    if(now.fin.eof()){
                        now.state=DONE;
                        continue;
                    }
                    now.fin.getline(now.writebuf,writebuf_len-1);
                    if(now.fin.eof()){
                        now.state=DONE;
                        continue;
                    }
                    now.writesize=strlen(now.writebuf);
                    if(!now.fin.fail()||(now.fin.fail()&&now.writesize==0)){
                        now.writebuf[now.writesize]='\n';
                        now.writesize++;
                        now.writebuf[now.writesize]=0;
                    }
                    now.fin.clear();
                    print(now.writebuf,i,true);
                }
                int size=write(now.fd,now.writebuf+now.writeidx,now.writesize-now.writeidx);
                if(size<0)throw("Failed in write");
                now.writeidx+=size;
                if(now.writeidx==now.writesize&&now.writebuf[now.writesize-1]=='\n'){
                    cerr << "finished a line switch to read" << endl;
                    now.state=READING;
                }
            }
            else if(now.state==READING&&FD_ISSET(now.fd,&rfds)){
                char readbuf[readbuf_len];
                int cnt;
                while((cnt=read(now.fd,readbuf,readbuf_len-1))>0){
                    readbuf[cnt]=0;
                    print(readbuf,i,false);
                    if(any_of(readbuf,readbuf+cnt,[](char x){return x=='%';}))
                        now.state=WRITING;
                }
                if(cnt==-1){
                    if(errno!=EAGAIN&&errno!=EWOULDBLOCK)throw("Error in read");
                }
                else{
                    cerr << "Grace close " << i << endl;
                    now.state=DONE;
                    continue;
                }
            }
        }
    }
}

int main (){
    try{
        printheader();
        parsequery();
        printbody();
        setupconnections();
        handlesockets();
        printfooter();
    }
    catch (const char* err){
        cout << err << endl;
    }

}
