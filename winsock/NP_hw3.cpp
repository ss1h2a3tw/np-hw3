#include <windows.h>
#include <list>
#include <cstdint>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>
#include "resource.h"
#define WM_SOCKET_NOTIFY (WM_USER + 1)
using namespace std;
BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);


const uint16_t SERVER_PORT=8790;

const size_t query_length=65536;
const size_t readbuf_len=65536;
const size_t writebuf_len=65536;

enum SOCKSTATE{CONNECTING,WRITING,READING,DONE,HTTP_READ,HTTP_WRITE,HTTP_DONE};


struct conn{
    SOCKSTATE state;
    stringstream sswritebuf;
    char writebuf[writebuf_len];
    size_t writebufsize;
    size_t writebufidx;
    //CGI CLIENT
    FILE* fin;
    SOCKET httpsock;
	int id;
    //HTTP SERVER
    int cgi_con;
	bool is_cgi;
    conn():writebufsize(0),writebufidx(0) {};
	//conn(const conn&) = delete;
	//conn& operator=(const conn&) = delete;
	conn(const conn& x){
		writebufsize=0;
		writebufidx=0;
	}
	conn& operator=(const conn&){
		writebufsize=0;
		writebufidx=0;
		return *this;
	}
};

map<SOCKET,conn> connects;

void print(stringstream& ss,const char* str,int id,bool bold){
    ss << "<script>document.all['m" << id << "'].innerHTML += \"";
    if(bold)ss << "<b>";
    for(size_t i = 0 ; i < strlen(str) ; i ++){
        if(str[i] == '\n') ss << "<br>";
        else if(str[i] == '\r');
        else if(str[i] == '<') ss << "&lt;";
        else if(str[i] == '>') ss << "&gt;";
        else if(str[i] == '&') ss << "&amp;";
        else if(str[i] == '"') ss << "&quot;";
        else if(str[i] == ' ') ss << "&nbsp;";
        else ss << str[i];
    }
    if(bold)ss << "</b>";
    ss << "\";</script>";
}

void printheader(stringstream& ss){
    ss << "HTTP/1.1 200 OK\r\n";
    ss << "Status: 200\r\n";
    ss << "Content-Type: text/html\r\n\r\n";
    ss << "\
<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\
<title>Network Programming Homework 3</title></head><body bgcolor=#336699>\
<font face=\"Courier New\" size=2 color=#FFFF99>";
}

void printbody(stringstream& ss,const vector<string>& hosts){
    ss <<"<table width=\"800\" border=\"1\"><tr>";
    for(size_t i = 0 ; i < hosts.size() ; i ++){
        ss << "<td>" << hosts[i] << "</td>";
    }
    ss << "</tr><tr>";
    for(size_t i = 0 ; i < hosts.size() ; i ++){
        ss << "<td valign=\"top\" id=\"m" << i << "\"></td>";
    }
    ss << "</tr></table>";
}

void printfooter(stringstream& ss){
    ss << "</font></body></html>";
}

void processCGI(const string& _query,SOCKET httpsock,HWND& hwnd,HWND& hwndEdit){
    stringstream& httpss=connects[httpsock].sswritebuf;
    printheader(httpss);
    char query[query_length];
    strncpy(query,_query.c_str(),query_length);
    map<string,string> para;
    query[query_length-1]=0;
    {
        size_t pre = 0;
        auto len = strlen(query);
        query[len]='&';
        for(size_t i = 0 ; i <= len ; i ++){
            if(query[i]=='&'){
                for(size_t j = pre ; j < i ; j ++){
                    if(query[j]=='='){
                        query[j]=0;
                        query[i]=0;
                        para[string(query+pre)]=string(query+j+1);
                        break;
                    }
                }
                pre=i+1;
            }
        }
    }
    vector<string> hosts;
    for(int i = 1 ; i <= 5; i ++){
        const string h=string("h")+to_string((long long )i);
        const string p=string("p")+to_string((long long )i);
        const string f=string("f")+to_string((long long )i);
        if(para.count(h)&&para.count(p)&&para.count(f)){
            SOCKET now = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(stoi(para[p]));
            addr.sin_addr.s_addr=inet_addr(para[h].c_str());
			u_long val = 1;
            ioctlsocket(now, FIONBIO, &val);
            connect(now,(sockaddr*)&addr,sizeof(addr));
            connects[now].state=READING;
            connects[now].fin=fopen(para[f].c_str(),"r");
			//if(connects[now].fin==NULL)EditPrintf(hwndEdit,TEXT("NULL!"));
            connects[now].writebufidx=0;
            connects[now].writebufsize=0;
            connects[now].httpsock=httpsock;
			connects[now].id=i-1;
			//EditPrintf(hwndEdit, TEXT("===one cgi con=="));
            hosts.push_back(para[h]);
            int err = WSAAsyncSelect(now, hwnd, WM_SOCKET_NOTIFY, FD_READ);
            if ( err == SOCKET_ERROR ) {
                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                closesocket(now);
                WSACleanup();
            }
            connects[httpsock].cgi_con++;
        }
    }
    printbody(httpss,hosts);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    WSADATA wsaData;

    static HWND hwndEdit;
    static SOCKET msock, ssock;
    static struct sockaddr_in sa;

    int err;


    switch(Message)
    {
        case WM_INITDIALOG:
            hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
            break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case ID_LISTEN:
                    WSAStartup(MAKEWORD(2, 0), &wsaData);
                    //create master socket
                    msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
                    if( msock == INVALID_SOCKET ) {
                        EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
                        WSACleanup();
                        return TRUE;
                    }
                    err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);
                    if ( err == SOCKET_ERROR ) {
                        EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                        closesocket(msock);
                        WSACleanup();
                        return TRUE;
                    }
                    //fill the address info about server
                    sa.sin_family       = AF_INET;
                    sa.sin_port         = htons(SERVER_PORT);
                    sa.sin_addr.s_addr  = INADDR_ANY;
                    //bind socket
                    err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));
                    if( err == SOCKET_ERROR ) {
                        EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
                        WSACleanup();
                        return FALSE;
                    }
                    err = listen(msock, 2);
                    if( err == SOCKET_ERROR ) {
                        EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
                        WSACleanup();
                        return FALSE;
                    }
                    else {
                        EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
                    }

                    break;
                case ID_EXIT:
                    EndDialog(hwnd, 0);
                    break;
            };
            break;

        case WM_CLOSE:
            EndDialog(hwnd, 0);
            break;

        case WM_SOCKET_NOTIFY:
			{
			conn& nowconn=connects[wParam];
            const SOCKET& nowsock=wParam;
            switch( WSAGETSELECTEVENT(lParam) )
            {
                case FD_ACCEPT:
                    ssock = accept(msock, NULL, NULL);
                    connects[ssock].state=HTTP_READ;
                    connects[ssock].cgi_con=0;
                    connects[ssock].sswritebuf.str("");
					connects[ssock].writebufidx=0;
					connects[ssock].writebufsize=0;
                    EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), Connects size:%d ===\r\n"), ssock, connects.size());
                    err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
                    if ( err == SOCKET_ERROR ) {
                        EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                        closesocket(msock);
                        WSACleanup();
                        return TRUE;
                    }
                    break;
                case FD_READ:
                    if(nowconn.state==HTTP_READ){
                        //Expect the HTTP request's first line will be lesser than readbuf;
                        char readbuf[readbuf_len]={0};
                        int ret=recv(nowsock,readbuf,readbuf_len-1,0);
						EditPrintf(hwndEdit,TEXT("HTTP_READ\n"));
                        if(ret<0){
                            closesocket(nowsock);
                            return TRUE;
                        }
                        stringstream ss;
                        ss << readbuf;
                        string method,uri;
                        ss >> method >> uri;
                        string query;
                        if(uri.find("?")!=string::npos){
                            query=uri.substr(uri.find("?")+1);
                            uri=uri.substr(0,uri.find("?"));
                        }
						if(uri.find("http")!=string::npos){
							size_t pos=0;
							for(int i = 0 ; i < 3 ; i ++){
								if(pos==string::npos)throw("Error Parsing URI");
								pos=uri.find_first_of("/",pos+1);
							}
							if(pos==string::npos)throw("Error Parsing URI");
							uri=uri.substr(pos);
						}
						uri=uri.substr(1);
						EditPrintf(hwndEdit,TEXT("URI %s\n"),uri.c_str());
                        bool iscgi = uri.rfind(".cgi")==uri.size()-4;
                        if(method!="GET"){
                            closesocket(nowsock);
                            return TRUE;
                        }
						if(!iscgi){
							nowconn.is_cgi=false;
							EditPrintf(hwndEdit,TEXT("Not CGI"));
							ifstream fin;
							fin.open(uri);
							fin >> noskipws;
							if(fin.fail()){
								closesocket(nowsock);
								return TRUE;
							}
							nowconn.sswritebuf << "HTTP/1.1 200 OK\r\n\r\n";
							while(1){
								char c;
								fin >> c;
								if(fin.eof())break;
								nowconn.sswritebuf << c;
							}
							nowconn.state=HTTP_DONE;
						}
						else{
							nowconn.is_cgi=true;
							EditPrintf(hwndEdit,TEXT("Is CGI"));
							processCGI(query,nowsock,hwnd,hwndEdit);
							nowconn.state=HTTP_WRITE;
						}
						err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
						if ( err == SOCKET_ERROR ) {
							EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
							closesocket(nowsock);
							WSACleanup();
							return TRUE;
						}
                    }
                    else if(nowconn.state==READING){
                        char readbuf[readbuf_len];
                        int size;
                        bool haveprompt=false;
                        while((size=recv(nowsock,readbuf,readbuf_len-1,0))>0){
                            readbuf[size]=0;
                            if(any_of(readbuf,readbuf+size,[](char x){return x=='%';}))
                                haveprompt=true;
                            print(connects[nowconn.httpsock].sswritebuf,readbuf,nowconn.id,false);
                        }
                        if(size==0){
                            nowconn.state=DONE;
                            connects[nowconn.httpsock].cgi_con--;
                            closesocket(nowsock);
							shutdown(nowsock,2);
							EditPrintf(hwndEdit,"grace close");
                            err = WSAAsyncSelect(nowconn.httpsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowconn.httpsock);
                                WSACleanup();
                                return TRUE;
                            }
                            return TRUE;
                        }
                        else if (size==SOCKET_ERROR){
                            err=WSAGetLastError();
                            if(err!=WSAEWOULDBLOCK){
                                closesocket(nowsock);
                                return TRUE;
                            }
							//EditPrintf(hwndEdit,TEXT("Done reading\n"));
                        }
						//if(haveprompt)EditPrintf(hwndEdit,TEXT("Have prompt"));
                        if(haveprompt){
							err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
							nowconn.state=WRITING;
						}
                        else err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
                        if ( err == SOCKET_ERROR ) {
                            EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                            closesocket(nowsock);
                            WSACleanup();
                            return TRUE;
                        }
                        err = WSAAsyncSelect(nowconn.httpsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                        if ( err == SOCKET_ERROR ) {
                            EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                            closesocket(nowconn.httpsock);
                            WSACleanup();
                            return TRUE;
                        }
                    }
                    break;
                case FD_WRITE:
                    if(nowconn.state==WRITING){
                        if(nowconn.writebufidx>=nowconn.writebufsize){
                            if(feof(nowconn.fin)){
                                err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
                                if ( err == SOCKET_ERROR ) {
                                    EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                    closesocket(nowsock);
                                    WSACleanup();
                                    return TRUE;
                                }
                                return TRUE;
                            }
                            fgets(nowconn.writebuf,writebuf_len,nowconn.fin);
							if(feof(nowconn.fin)){
                                err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
                                if ( err == SOCKET_ERROR ) {
                                    EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                    closesocket(nowsock);
                                    WSACleanup();
                                    return TRUE;
                                }
                                return TRUE;
                            }
                            print(connects[nowconn.httpsock].sswritebuf,nowconn.writebuf,nowconn.id,true);
                            nowconn.writebufsize=strlen(nowconn.writebuf);
                            nowconn.writebufidx=0;
                            err = WSAAsyncSelect(nowconn.httpsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowconn.httpsock);
                                WSACleanup();
                                return TRUE;
                            }
                        }
                        int size=send(nowsock,nowconn.writebuf+nowconn.writebufidx,nowconn.writebufsize-nowconn.writebufidx,0);
                        if(size<0)EditPrintf(hwndEdit, TEXT("Error in send\n"));
                        nowconn.writebufidx+=size;
                        if(nowconn.writebufidx>=nowconn.writebufsize && nowconn.writebuf[strlen(nowconn.writebuf)-1]=='\n'){
                            nowconn.state = READING;
                            err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowsock);
                                WSACleanup();
                                return TRUE;
                            }
                            return TRUE;
                        }
                        else {
                            err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowsock);
                                WSACleanup();
                                return TRUE;
                            }
                            return TRUE;
                        }
                    }
                    else if(nowconn.state==HTTP_WRITE){
                        if(nowconn.writebufidx>=nowconn.writebufsize){
                            nowconn.sswritebuf.getline(nowconn.writebuf,writebuf_len-1);
							nowconn.writebufsize=strlen(nowconn.writebuf);
							if(!nowconn.sswritebuf.fail()){
								nowconn.writebuf[nowconn.writebufsize]='\n';
								nowconn.writebufsize++;
								nowconn.writebuf[nowconn.writebufsize]=0;
							}
                            nowconn.sswritebuf.clear();
                            nowconn.writebufidx=0;
                        }
                        if(nowconn.writebufsize!=0){
                            int size=send(nowsock,nowconn.writebuf+nowconn.writebufidx,nowconn.writebufsize-nowconn.writebufidx,0);
                            if(size<0)EditPrintf(hwndEdit, TEXT("Error in send\n"));
                            nowconn.writebufidx+=size;
                            err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowsock);
                                WSACleanup();
                                return TRUE;
                            }
                        }
                        if(nowconn.cgi_con==0){
							EditPrintf(hwndEdit,TEXT("Prepare to close http"));
                            if(nowconn.is_cgi)printfooter(nowconn.sswritebuf);
                            nowconn.state=HTTP_DONE;
                        }
                    }
                    else if(nowconn.state==HTTP_DONE){
                        if(nowconn.writebufidx>=nowconn.writebufsize){
                            nowconn.sswritebuf.getline(nowconn.writebuf,writebuf_len-1);
							nowconn.writebufsize=strlen(nowconn.writebuf);
							if(!nowconn.sswritebuf.fail()){
								nowconn.writebuf[nowconn.writebufsize]='\n';
								nowconn.writebufsize++;
								nowconn.writebuf[nowconn.writebufsize]=0;
							}
							nowconn.sswritebuf.clear();
                            nowconn.writebufidx=0;
							EditPrintf(hwndEdit,TEXT("get new write buf size%d %s\n"),nowconn.writebufsize,nowconn.writebuf);
                        }
                        if(nowconn.writebufsize!=0){
                            int size=send(nowsock,nowconn.writebuf+nowconn.writebufidx,nowconn.writebufsize-nowconn.writebufidx,0);
                            if(size<0)EditPrintf(hwndEdit, TEXT("Error in send\n"));
                            nowconn.writebufidx+=size;
                            err = WSAAsyncSelect(nowsock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
                            if ( err == SOCKET_ERROR ) {
                                EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
                                closesocket(nowsock);
                                WSACleanup();
                                return TRUE;
                            }
                        }
                        else {
                            closesocket(nowsock);
                        }
                    }
                    break;
                case FD_CLOSE:
                    break;
            };
			}break;
        default:
            return FALSE;

    };

    return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
    TCHAR   szBuffer [1024] ;
    va_list pArgList ;

    va_start (pArgList, szFormat) ;
    wvsprintf (szBuffer, szFormat, pArgList) ;
    va_end (pArgList) ;

    SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
    SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
    SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
    return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0);
}